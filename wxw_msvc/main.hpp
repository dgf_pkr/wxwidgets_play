#ifndef MAIN_HPP_INCLUDED
#define MAIN_HPP_INCLUDED

#include <wx/wx.h>
class MainApp : public wxApp
{
public:
    virtual bool OnInit();
    virtual int OnExit();
};

DECLARE_APP(MainApp)




#endif // MAIN_HPP_INCLUDED
