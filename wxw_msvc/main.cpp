#include "main.hpp"
#include "helloWorld.hpp"
#include <wx/sharedptr.h>


IMPLEMENT_APP(MainApp)


bool MainApp::OnInit()
{
    MyFrame *p_myFrame=new MyFrame(wxT("This is a test"));
    p_myFrame->Show(!true);
    TextFrame *p_textFrame=new TextFrame(wxT("Title"), 0, 0, 100, 1000);
    p_textFrame->Show(true);
    p_textFrame->CreateStatusBar(3);

    
    return true;
}
int MainApp::OnExit()
{
    return wxApp::OnExit();
}
