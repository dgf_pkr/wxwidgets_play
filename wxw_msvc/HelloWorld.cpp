// ___     ___   ___         ___   _  __  ___ 
//|   \   / __| | __|       | _ \ | |/ / | _ \
 //| |) | | (_ | | _|        |  _/ | ' <  |   /
//|___/   \___| |_|    ___  |_|   |_|\_\ |_|_\
 //                    |___|     
// ASCII ART thanks to http://www.patorjk.com/software/taag/ Font used = "small". Character Width = Full. Height = Defualt


//Adapted from http://docs.wxwidgets.org/stable/overview_helloworld.html
#include "helloWorld.hpp"
#include <wx/sharedptr.h>

MyFrame::MyFrame(const wxString& title)
    : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(250, 150))
{
    Centre();
}

MyFrame::~MyFrame() {}

void MyFrame::OnHello(wxCommandEvent &event)
{
    wxLogMessage("Hello World!");

}
void MyFrame::OnExit(wxCommandEvent &event)
{
    Close(true);
}

enum ID
{
    ID_Hello =1
};

wxBEGIN_EVENT_TABLE(MyFrame, wxFrame)
EVT_MENU(ID_Hello, MyFrame::OnHello)
wxEND_EVENT_TABLE()

TextFrame::TextFrame(const wxChar *title, int xpos, int ypos, int width, int height)
    : wxFrame((wxFrame *)NULL, -1, title, wxPoint(xpos, ypos), wxSize(width, height))
{
    m_pTextCtrl=new wxTextCtrl(this, -1, _T("Type some text..."),
        wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
    m_pMenuBar=new wxMenuBar();
    m_pFileMenu=new wxMenu();
    m_pFileMenu->Append(wxID_OPEN, _T("&Open"), _T("Opens an existing file"));
    m_pFileMenu->Append(wxID_SAVE, _T("&Save"));
    m_pFileMenu->AppendSeparator();
    m_pFileMenu->Append(wxID_EXIT, _T("&Quit"));
    
    m_pMenuBar->Append(m_pFileMenu, _T("&File"));

    m_pHelpMenu = new wxMenu();
    m_pHelpMenu->Append(wxID_ABOUT, _T("&About"));

    m_pMenuBar->Append(m_pHelpMenu, _T("&Help"));

    SetMenuBar(m_pMenuBar);
}
