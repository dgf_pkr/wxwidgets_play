 // ___     ___   ___         ___   _  __  ___ 
 //|   \   / __| | __|       | _ \ | |/ / | _ \
 //| |) | | (_ | | _|        |  _/ | ' <  |   /
 //|___/   \___| |_|    ___  |_|   |_|\_\ |_|_\
 //                    |___|     
 // ASCII ART thanks to http://www.patorjk.com/software/taag/ Font used = "small". Character Width = Full. Height = Defualt


//Adapted from http://docs.wxwidgets.org/stable/overview_helloworld.html.


#ifndef HELLOWORLDAGAIN__H
#define HELLOWORLDAGAIN__H
#include <wx/wx.h>

class MyFrame : public wxFrame
{
public:
    MyFrame(const wxString& title);
    virtual ~MyFrame();
private:
    void OnHello(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);
    wxDECLARE_EVENT_TABLE();
};

class TextFrame : public wxFrame
{
public:
    TextFrame(const wxChar *title, int xpos, int ypos, int width, int height);
private:
    wxTextCtrl *m_pTextCtrl;
    wxMenuBar *m_pMenuBar;
    wxMenu *m_pFileMenu;
    wxMenu *m_pHelpMenu;
};



#endif // HELLOWORLDAGAIN__H
